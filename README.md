
# Utiliser GeoDjango avec Docker et Postgresql 

## Installation Django 

*  [compose.yaml](compose.yaml) 

*  Créer le projet Django :   
`[sudo] docker-compose run <web / api> django-admin startproject <nom_projet> . ` 

*  Lancer le container et créer l'image :  
`docker-compose up [--build]`  --> 'build' est lancé si l'image n'existe pas déjà, même si on ne le lance pas explicitement. 


## Installer les outils et les bibliothèques géospatiales 

### Ressources web 

#### GeoDjango 
https://docs.djangoproject.com/fr/4.1/ref/contrib/gis/install/  

#### Bibliothèques géospatiales 
https://docs.djangoproject.com/fr/4.1/ref/contrib/gis/install/geolibs/  
  
#### Lien utile (table des versions)  
https://trac.osgeo.org/postgis/wiki/UsersWikiPostgreSQLPostGIS   
  


### Installations 
https://docs.djangoproject.com/fr/4.1/ref/contrib/gis/install/geolibs/  

**Note**  
Les interfaces GeoDjango vers GEOS, GDAL et GeoIP peuvent être utilisées indépendamment de Django. Autrement dit, il n’est pas nécessaire d’avoir une base de données ou un fichier de réglages, importez-les comme un module normal à partir de django.contrib gis.


#### GEOS 

GEOS est une bibliothèque C++ destinée aux opérations géométriques et constitue la représentation géométrique interne par défaut que GeoDjango utilise (derrière les objets géométriques « différés »)

- installer cmake :  
`[sudo] apt install cmake`  

- télécharger geos : voir table des versions  

- extraire les fichiers  

- **build** :   
suivre les instructions de cette page :  
https://trac.osgeo.org/geos/wiki/BuildingOnUnixWithCMake  
**Voir "Configure"**  


*  **BDD GIS** :  
PostGIS nécessite que Geos, Proj et Gdal soient installés avant de compiler PostGIS. 
Pour PostGIS :  
https://postgis.net/install/ 


#### Proj 

https://proj.org/install.html  


#### Gdal 

https://gdal.org/development/building_from_source.html  

**Warning**
When iterating to configure GDAL to add/modify/remove dependencies, some cache variables can remain in CMakeCache.txt from previous runs, and conflict with new settings. If strange errors appear during cmake run, you may try removing CMakeCache.txt to start from a clean state.  

**Note**
For a minimal build, add these options to the initial `cmake` command:  
`-DGDAL_BUILD_OPTIONAL_DRIVERS=OFF -DOGR_BUILD_OPTIONAL_DRIVERS=OFF`. 
To enable specific drivers, add   `-DGDAL_ENABLE_DRIVER_<driver_name>=ON` or `-DOGR_ENABLE_DRIVER_<driver_name>=ON`.  
See Selection of drivers for more details.



**Note**  
A default build is not optimized without specifying -DCMAKE_BUILD_TYPE=Release (or similar) during configuration, or by specifying --config Release with CMake multi-configuration build tools (see example below).  





## Créer un superutilisateur (quand tous les outils GIS sont installés)  

*  Entrer dans le container :  
`docker exec -it <projet>_api_1 bash`  

*  Créer un superutilisateur :  
`python manage.py createsuperuser`  
Renseigner le username, le mail et le mot de passe 2 fois.  


*  Pour se connecter, aller sur localhost:8000/admin/  




