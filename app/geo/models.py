from django.db import models

class Address(models.Model): 
    ad_code = models.CharField(max_length=254) 
    ad_ban_id = models.CharField( 
        max_length=24, 
        blank = True,
        null = True
    ) 
    ad_nomvoie = models.CharField( 
        max_length=254 
    ) 
    ad_fantoir = models.CharField( 
        max_length=10, 
        blank = True,
        null = True 
    ) 
    ad_numero = models.IntegerField() 
    ad_rep = models. CharField( 
        max_length=20, 
        blank = True,
        null = True 
    ) 
    ad_insee = models.CharField( 
        max_length=6, 
        blank = True,
        null = True 
    ) 
    ad_postal = models.CharField( 
        max_length=20 
    ) 
    ad_commune = models.CharField( 
        max_length=254, 
        blank = True,
        null = True 
    ) 

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True) 

    def __str__(self): 
        return f'{self.ad_commune}' 

